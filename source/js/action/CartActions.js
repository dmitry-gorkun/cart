
import AppDispatcher from "../dispatcher/AppDispatcher";
import CartConstants from "../constants/CartConstants";

class CartActions {

    // Изменение количества в товаре
    static changeProduct(index, count) {
        AppDispatcher.dispatch({
            actionType: CartConstants.CART_CHANGE,
            index: index,
            count: count
        });
    }

    // Добавляем товар в корзину
    static addProduct(item) {
        AppDispatcher.dispatch({
            actionType: CartConstants.CART_ADD,
            item: item
        });
    }

    // Удаляем товар из корзины
    static removeProduct(index) {
        AppDispatcher.dispatch({
            actionType: CartConstants.CART_REMOVE,
            index: index
        });
    }

    // Сортируем корзину
    static sortCart(key) {
        AppDispatcher.dispatch({
            actionType: CartConstants.CART_SORT,
            key: key
        });
    }

    // Событие очистки корзины полностью
    static clearCart(key) {
        AppDispatcher.dispatch({
            actionType: CartConstants.CART_CLEAR,
            key: key
        });
    }
}

export default CartActions;