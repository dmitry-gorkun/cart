
import AppDispatcher from "../dispatcher/AppDispatcher";
import DataConstants from "../constants/DataConstants";

class DataActions {

    // Событие загрузки данных
    static loadData(data) {
        AppDispatcher.dispatch({
            actionType: DataConstants.DATA_LOAD,
            data: data
        });
    }
}

export default DataActions;