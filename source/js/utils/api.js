import request from "superagent";
import DataActions from "../action/DataActions";

var API = {
    // Загружаем фейковые данные из заглушки
    getData: function() {
        request
            .get('data.json')
            .end(function(error, response) {
                if (!error) {
                    var data = JSON.parse(response.text);
                    DataActions.loadData(data);
                }
            });
    }
};

export default API;
