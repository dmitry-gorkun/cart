import keyMirror from "keymirror";

var CartConstants = keyMirror({
    CART_ADD: null,     // Добавление товара в корзину
    CART_REMOVE: null,  // Удаление товара из корзины
    CART_SORT: null,    // Стабильная сортировка по ключу
    CART_CLEAR: null,    // Очистка корзины
    CART_CHANGE: null    // Изменение одного из товаров (количество)
});

export default CartConstants;