import keyMirror from "keymirror";

var DataConstants = keyMirror({
    DATA_LOAD: null  // Загрузка списка товаров
});

export default DataConstants;