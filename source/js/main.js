import React from 'react';
import ReactDOM from 'react-dom';
import API from './utils/api';
import CartApp from './components/CartApp.react';

// Загружаем данные
API.getData();

// Рендерим
ReactDOM.render(<CartApp />, document.getElementById('main'));