import React from 'react';
import Cart from './Cart.react';
import Data from './Data.react';
import BuyButton from './BuyButton.react';
import CartStore from '../stores/CartStore';

class CartApp extends React.Component {

    // Подгружаем в состояние данные для корзины и
    // из заглушки для генерации новых товаров
    state = {
        cart: CartStore.getData()
    };

    componentDidMount() {
        CartStore.addChangeListener(this._onChange.bind(this));
    }

    componentWillUnmount() {
        CartStore.removeChangeListener(this._onChange.bind(this));
    }

    _onChange() {
        this.setState({
            cart: CartStore.getData()
        });
    }

    render() {
        return (
            <div className="cart-app">
                <Data />
                <Cart cart={this.state.cart}/>
                <BuyButton cart={this.state.cart}/>
            </div>
        );
    }
}

export default CartApp;
