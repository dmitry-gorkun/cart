import React from 'react';
import CartActions from '../action/CartActions';
import DataActions from '../action/DataActions';
import DataStore from '../stores/DataStore';

class Data extends React.Component {

    // Добавление произвольного товара из заглушки
    handleAddToCart() {
        var items = DataStore.getData(),
            rand = Math.floor(Math.random() * items.length);

        if (items.length > 0) {
            CartActions.addProduct(items[rand]);
        }
    }

    render() {
        return (
            <div className="cart-right">
                <a className="cart-btn" onClick={this.handleAddToCart.bind(this)} title="Добавить в корзину">
                    <i className="icon-plus"></i>
                    <span>&nbsp;Добавить в корзину</span>
                </a>
            </div>
        );
    }
}

export default Data;