
import React from "react";
import CartActions from "../action/CartActions";

var ReactPropTypes = React.PropTypes;

class CartItem extends React.Component {

    static defaultProps = {
        index: ReactPropTypes.number.isRequired,
        title: ReactPropTypes.string,
        price: ReactPropTypes.number
    };

    // Изменение товара в корзине
    handleChangeCount(e) {
        CartActions.changeProduct(this.props.index, {
            count: e.target.value,
            title: this.props.title,
            price: this.props.price
        });
    }

    // Удаление (((
    handleRemove(index) {
        CartActions.removeProduct(index);
    }

    render() {

        var { price, title, index, count} = this.props;

        return (
            <tr className="cart-row" data-index={index}>
                <td className="cart-col ta-right">
                    <span className="cart-remove-btn" title="Remove" onClick={this.handleRemove.bind(this, index)}>
                        <i className="icon-cancel"></i>
                    </span>
                </td>
                <td>{title}</td>
                <td className="cart-col ta-right">{price}</td>
                <td className="cart-col ta-right">
                    <input type="number" value={count} min="1" max="10" onChange={this.handleChangeCount.bind(this)} className="cart-input" />
                </td>
            </tr>
        );
    }
}

export default CartItem;