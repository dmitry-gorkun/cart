import React from "react";
import CartActions from "../action/CartActions";
import CartItem from "./CartItem.react";

class Cart extends React.Component {

    // Сортировка корзины
    handleSortByKey(key) {
        CartActions.sortCart(key);
    }

    render() {

        var items = [], cart = this.props.cart;

        cart.items.forEach((v, i)  => items.push(<CartItem key={i} index={i} price={v.price} title={v.title} count={v.count} />));

        return (
            <div>
                <table className="cart-table pure-table-horizontal fullwidth">
                    <thead>
                        <tr className="cart-row">
                            <th className="cart-col">&nbsp;</th>
                            <th className="cart-col">
                                <span onClick={this.handleSortByKey.bind(this, 'title')} className="cart-sort-switcher">
                                    <span>Название</span>
                                    <i className={cart.sort.key === 'title' ? 'icon-' + cart.sort.order : 'hidden'}></i>
                                </span>
                            </th>
                            <th className="cart-col ta-right">
                                <span onClick={this.handleSortByKey.bind(this, 'price')} className="cart-sort-switcher">
                                    <span>Цена</span>
                                    <i className={cart.sort.key === 'price' ? 'icon-' + cart.sort.order  : 'hidden'}></i>
                                </span>
                            </th>
                            <th className="cart-col ta-right">
                                <span onClick={this.handleSortByKey.bind(this, 'count')}  className="cart-sort-switcher">
                                    <span>Количество</span>
                                    <i className={cart.sort.key === 'count' ? 'icon-' + cart.sort.order  : 'hidden'}></i>
                                </span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>{items}</tbody>
                </table>
                <div className="cart-row cart-right">Итого: {cart.total} руб.</div>
            </div>
        );
    }
}

export default Cart;