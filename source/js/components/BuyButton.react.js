import React from 'react';
import CartActions from '../action/CartActions';

class BuyButton extends React.Component {

    handleBuyAll(e) {
        var cart = this.props.cart;

        // Здесь должна быть заглушка для отправки на сервер
        alert(`Куплено ${cart.items.length} наименований товаров на сумму ${cart.total}`);

        e.preventDefault();

        CartActions.clearCart();
    }

    render() {
       return (
           <div className="cart-right">
               <a id="buy-btn" className="cart-btn" href="#" onClick={this.handleBuyAll.bind(this)}  title="Buy">
                   <i className="icon-basket"></i>
                   <span>Купить</span>
               </a>
           </div>
       )
    }
}

export default BuyButton;