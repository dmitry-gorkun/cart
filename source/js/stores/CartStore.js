import AppDispatcher from "../dispatcher/AppDispatcher";
import CartConstants from "../constants/CartConstants";
import _ from 'underscore';
import MicroEvent from 'microevent';

var CartStore = {

    _data: {
        // Товары в корзине
        items: [],
        // Параметры сортировки корзины
        sort: {
            key: 'title',
            order: 'up'
        },
        total: 0
    },


    // Получаем данные из локального хранилища
    // Или грузим значение по умолчанию.
    getData() {
        var memory = window.localStorage.getItem("cart");

        this._data = JSON.parse(memory) || this._data;
        return this._data;
    },

    // Пересчет общей суммы
    calcTotal() {
        this._data.total = this._data.items.reduce((mem, i) => mem + i.price * i.count, 0).toFixed(2);
    },

    // Добавление нового продукта в корзину
    add(item) {

        var el = _.where(this._data.items, {
            sku: item.sku
        });

        if (el.length > 0) {
            el[0].count++;
        } else {
            this._data.items.push({
                sku: item.sku,
                title: item.title,
                price: item.price,
                count: 1
            });
        }

        this.save();
    },

    // Изменение состояния товара в корзине
    changeOrderItem(index, item) {
        this._data.items[index] = item;
        this.save();
    },

    // Удаление товара из корзины
    remove(index) {
        this._data.items.splice(index, 1);
        this.save();
    },

    // Устойчивая сортировка с underscore :)
    sort(key) {

        // Сохраняем состояние сортировки
        var sort =  {
                key: key
            },
            ascending = _.sortBy(this._data.items, (el) => el[key]);

        // Выбираем тип сортировки
        sort.order = (this._data.sort.key !== key)
            ? 'asc'
            : ((this._data.sort.order === 'asc') ? 'desc' : 'asc');

        this._data.items = (sort.order === 'desc') ? ascending.reverse() : ascending;
        this._data.sort = sort;
        this.save();
    },

    // Полная очистка корзины
    clear() {
        this._data.items = [];
        this.save();
    },

    // Сохранение состояния в локальное хранилище
    save() {
        this.calcTotal();
        window.localStorage.setItem("cart", JSON.stringify(this._data));
        this.trigger('change');
    },

    addChangeListener(callback) {
        this.bind('change', callback);
    },

    removeChangeListener(callback) {
        this.unbind('change', callback);
    }
};

CartStore = _.extend(CartStore, MicroEvent.prototype);

AppDispatcher.register(function(payload) {

    switch( payload.actionType ) {
        case CartConstants.CART_ADD:
            CartStore.add(payload.item);
            break;
        case CartConstants.CART_REMOVE:
            CartStore.remove(payload.index);
            break;
        case CartConstants.CART_SORT:
            CartStore.sort(payload.key);
            break;
        case CartConstants.CART_CLEAR:
            CartStore.clear();
            break;
        case CartConstants.CART_CHANGE:
            CartStore.changeOrderItem(payload.index, payload.count);
            break;
    }

    return true;
});

export default CartStore;
