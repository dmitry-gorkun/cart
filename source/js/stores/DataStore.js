import AppDispatcher from "../dispatcher/AppDispatcher";
import DataConstants from "../constants/DataConstants";

var DataStore = {

    _data: [],

    getData() {
        return this._data;
    },

    setData(data) {
        this._data = data;
    }
};

AppDispatcher.register(function(payload) {

    switch( payload.actionType ) {
        case DataConstants.DATA_LOAD:
            DataStore.setData(payload.data);
            break;
    }

    return true;
});

export default DataStore;
