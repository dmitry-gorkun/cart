'use strict';

var
    gulp = require('gulp'),
    babelify = require('babelify'),
    browserify = require('browserify'),
    buffer = require('vinyl-buffer'),
    source = require('vinyl-source-stream'),
    size = require('gulp-size'),
    sourcemaps = require('gulp-sourcemaps'),
    plumber = require('gulp-plumber'),
    cssmin = require('gulp-cssnano'),
    less = require('gulp-less'),
    pathjs = require('path'),
    empty = require('gulp-empty'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
    mode = 'develop';

gulp.task('fonts:build', function() {
    return  gulp
        .src('source/font/**/*.*')
        .pipe(plumber())
        .pipe(gulp.dest('public_html/font/'))
        .pipe(reload({stream: true}));
});

gulp.task('others:build', function() {

    var srcs = [
        'source/html/index.html',
        'source/etc/data.json',
        'source/etc/robots.txt'
    ];

    return gulp.src(srcs)
        .pipe(gulp.dest('public_html/'))
        .pipe(reload({stream: true}));
});

gulp.task('css:build', function () {

    return gulp
        .src('source/less/style.less')
        .pipe(plumber())
        .pipe((mode !== 'production') ? sourcemaps.init('.') : empty())
        .pipe(less({
            paths: [
                pathjs.join(__dirname, 'less', 'includes')
            ]
        }))
        .pipe(cssmin({
            keepSpecialComments: 0
        }))
        .pipe(size({showFiles:true}))
        .pipe((mode !== 'production') ? sourcemaps.write('.') : empty())
        .pipe(gulp.dest('public_html/css/'))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function() {

    var bundler = browserify({
        entries: './source/js/main.js',
        debug: true
    });

    bundler.transform(babelify, {presets: ["es2015", "stage-1",  "react"]});

    return bundler.bundle()
        .pipe(source((mode !== 'production') ? 'build.js' : 'build.min.js'))
        .pipe(buffer())
        .pipe((mode !== 'production') ? sourcemaps.init('.') : empty())
        .pipe((mode == 'production') ? uglify() : empty())
        .pipe(size({showFiles:true}))
        .pipe((mode !== 'production') ? sourcemaps.write('./') : empty())
        .pipe(gulp.dest('public_html/js/'))
        .pipe(reload({stream: true}));
});

gulp.task('build', [
    'fonts:build',
    'js:build',
    'css:build',
    'others:build'
]);

gulp.task('production', function(){
    mode = 'production';
    gulp.start('build');
});

gulp.task('watch', function() {
    watch(['source/js/**/*.js'], function() {
        gulp.start('js:build');
    });
    watch(['source/less/**/*.less', 'source/css/**/*.css'], function() {
        gulp.start('css:build');
    });
    watch(['source/font/**/*.*'], function() {
        gulp.start('fonts:build');
    });
});

gulp.task('webserver', function () {
    browserSync({
        server: {
            baseDir: 'public_html'
        },
        host: 'exness.dev',
        port: 9011,
        logPrefix: "Frontend_KP"
    });
});

gulp.task('default', ['build', 'webserver', 'watch']);